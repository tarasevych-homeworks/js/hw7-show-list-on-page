
function arrayFunc(someArr) {
    const newArr = '<ul>' + (someArr.map(function (item) {
        return `<li>${item}</li>`
    }).join('')) + '</ul>';

    document.write(newArr);
}

const myArr = arrayFunc(['Margarita', 23, 34, true, 'Oldfashioned', null, false, 'Mai Tai']);
